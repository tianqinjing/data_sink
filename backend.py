#! /usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/9/12 13:29
# @File    : backend.py
# Note:
#    1. hbase存放的是byte字节序。所以Unicode最好使用encode转成字节序保存
#    2. hbase 所有数据必须是字符，不能是整形或者其他类型
#    入库时候注意一下

from base import Base
import time
from datetime import datetime
from sqlalchemy import Column, Integer, String, Boolean, Float, BLOB, DateTime


def dt2ts(datetime):
    # 转换 datatime 成为time.time
    if datetime is None:
        return ""

    if type(datetime) is int:
        return str(datetime)

    return int(time.mktime(datetime.timetuple()))


class Shop(Base):
    # 商家表
    __tablename__ = "organization"
    id = Column("id", Integer, nullable=False, primary_key=True)
    name = Column('name', String(255), doc=u'机构名')
    user_id = Column('user_id', Integer,  doc=u'创建机构用户ID')
    sup_organization_id = Column('sup_organization_id', Integer, doc=u'父机构名')
    create_time = Column("create_time", DateTime, default=datetime.now())
    update_time = Column("update_time", DateTime, default=datetime.now())

    def to_dict(self):
        return {
            "id": str(self.id),
            "name": self.name.encode('utf-8'),
            "user_id": str(self.user_id),
            "sup_organization_id": str(self.sup_organization_id),
            "create_time": str(dt2ts(self.create_time)),
            "update_time": str(dt2ts(self.update_time))
        }


class Device(Base):
    __tablename__ = "device"
    id = Column("id", String(255), nullable=False, primary_key=True, doc=u"设备ID")
    name = Column('name', String(255), nullable=False, doc=u'设备名')
    device_type_id = Column("device_type_id", Integer, doc=u"设备类型id")
    camera_ip = Column("camera_ip", String(255), doc=u"设备ip地址")
    port = Column("port", Integer, doc=u"设备端口")
    function = Column("function", String(255), doc=u"附属信息")
    screen_type = Column("screen_type", String(255), doc=u"标签")
    label = Column("label", String(255), doc=u"标签")
    organization_id = Column("organization_id", Integer, doc=u"机构id")
    status = Column("status", Integer, doc=u"状态")
    create_time = Column("create_time", DateTime, default=datetime.now())
    update_time = Column("update_time", DateTime, default=datetime.now())
    related_device_id = Column("related_device_id", String(255), doc=u"商显屏和摄像头")
    image_duration = Column("image_duration", Integer)

    def to_dict(self):
        data = {
            "id": str(self.id),
            "name": self.name.encode('utf-8'),
            "device_type_id": str(self.device_type_id),
            "camera_ip": self.camera_ip,
            "port": str(self.port),
            "function": self.function,
            "screen_type": str(self.screen_type),
            "label": self.label,
            "organization_id": str(self.organization_id),
            "status": str(self.status),
            "create_time": str(dt2ts(self.create_time)),
            "update_time": str(dt2ts(self.update_time)),
            "related_device_id": self.related_device_id,
            "image_duration": str(self.image_duration)
        }
        if self.function:
            data['function'] = self.function.encode('utf-8')
        return data


class DeviceType(Base):
    __tablename__ = "device_type"
    id = Column("id", Integer, primary_key=True)
    name = Column('name', String(255), nullable=False, doc=u'设备类型名')
    organization_id = Column("organization_id",  Integer, nullable=False, doc=u"机构ID")
    create_time = Column("create_time", DateTime, default=datetime.now())
    update_time = Column("update_time", DateTime, default=datetime.now())

    def to_dict(self):
        return {
            "id": str(self.id),
            "name": self.name.encode('utf-8'),
            "organization_id": self.organization_id,
            "create_time": str(dt2ts(self.create_time)),
            "update_time": str(dt2ts(self.update_time))
        }


class Person(Base):
    __tablename__ = "person"
    id = Column("id", Integer, nullable=False,primary_key=True,  doc=u"person_id_seq")
    name = Column('name', String(255), nullable=False, doc=u'姓名')
    bank_id = Column('bank_id', Integer, doc=u'bank_id')
    gender = Column('gender', Integer, doc=u'性别')
    id_card = Column('id_card', String(255),  doc=u'身份证号码')
    image_ids = Column('image_ids', String(255),  doc=u'图片id')
    activated = Column('activated', Integer,  doc=u'是否启用')
    remark = Column('remark', String(255),  doc=u'备注')
    created = Column("created", DateTime, default=datetime.now())
    updated = Column("updated", DateTime, default=datetime.now())

    def to_dict(self):
        return {
            "id": str(self.id),
            "name": self.name.encode('utf-8'),
            "bank_id": str(self.bank_id),
            "gender": str(self.gender),
            "id_card": self.id_card,
            "image_ids": self.image_ids,
            "activated": str(self.activated),
            "remark": self.remark,
            "created": str(dt2ts(self.created)),
            "updated": str(dt2ts(self.updated))
        }


class BankType(Base):
    __tablename__ = "bank_type"
    id = Column("id", Integer, primary_key=True, doc=u"bank_type_id_seq")
    name = Column('name', String(255), nullable=False, doc=u'库类型名称')
    organization_id = Column('organization_id', Integer, doc=u'所属机构Id')
    remark = Column('remark', String(255),  doc=u'备注信息')
    created = Column("created", DateTime, default=datetime.now())
    updated = Column("updated", DateTime, default=datetime.now())

    def to_dict(self):
        return {
            "id": str(self.id),
            "name": self.name.encode('utf-8'),
            "organization_id": str(self.organization_id),
            "remark": self.remark,
            "created": str(dt2ts(self.created)),
            "updated": str(dt2ts(self.updated))
        }


class Bank(Base):
    __tablename__ = "bank"
    id = Column("id", Integer, nullable=False, primary_key=True, doc=u"bank_id_seq")
    name = Column('name', String(255), nullable=False, doc=u'库类型名称')
    bank_type_id = Column('bank_type_id', Integer, nullable=False, doc=u'库类型Id')
    organization_id = Column('organization_id', Integer, doc=u'机构id')
    remark = Column('remark', String(255),  doc=u'备注信息')
    created = Column("created", DateTime, default=datetime.now())
    updated = Column("updated", DateTime, default=datetime.now())

    def to_dict(self):
        return {
            "id": str(self.id),
            "name": self.name.encode('utf-8'),
            "bank_type_id": str(self.bank_type_id),
            "organization_id": str(self.organization_id),
            "remark": self.remark,
            "created": str(dt2ts(self.created)),
            "updated": str(dt2ts(self.updated))
        }


class ContentType(Base):
    __tablename__ = "content_type"
    id = Column("id", Integer, nullable=False, primary_key=True,  doc=u"content_type_id_seq")
    name = Column('name', String(50), nullable=False, doc=u'广告类型')
    remark = Column('remark', String(120), nullable=False, doc=u'备注')
    organization_id = Column('organization_id', Integer, doc=u'机构ID')
    created = Column("created", DateTime, nullable=False, default=datetime.now())
    updated = Column("updated", DateTime, default=datetime.now())

    def to_dict(self):
        return {
            "id": str(self.id),
            "name": self.name.encode('utf-8'),
            "remark": self.remark,
            "organization_id": str(self.organization_id),
            "created": str(dt2ts(self.created)),
            "updated": str(dt2ts(self.updated))
        }


class Content(Base):
    __tablename__ = "content"
    id = Column("id", Integer, primary_key=True, nullable=False, doc=u"content_id_seq")
    name = Column('name', String(100), nullable=False, doc=u'广告标题')
    content_type_id = Column('content_type_id', Integer, nullable=False, doc=u'广告类型ID')
    horizontal_url = Column('horizontal_url', String(100), doc=u'水平广告URL')
    vertical_url = Column('vertical_url',String(100), doc=u'垂直广告URL')
    function = Column('function', String(255), doc=u'水平广告URL')
    organization_id = Column('organization_id', Integer, doc=u'机构ID')
    activated = Column('activated', Integer, doc=u'是否启用')
    label_ids = Column('label_ids', String(255), doc=u'标签组合，逗号隔开')
    play_mode = Column('play_mode', Integer, doc=u'播放模式')
    periods = Column('periods', String(255), doc=u'')
    billing_type_play_duration = Column('billing_type_play_duration', Integer, doc=u'机构ID')
    billing_type_play_times = Column('billing_type_play_times', Integer, doc=u'机构ID')
    billing_type_watch_effect = Column('billing_type_watch_effect', String(255), doc=u'')
    device_id = Column('device_id', String(255), doc=u'')
    created = Column("created", DateTime, nullable=False, default=datetime.now())
    updated = Column("updated", DateTime, default=datetime.now())
    start = Column("start", DateTime, nullable=False, default=datetime.now())
    finish = Column("finish", DateTime, default=datetime.now())
    play_times = Column("play_times", Integer, default=0)

    def to_dict(self):
        return {
            "id": str(self.id),
            "name": self.name.encode('utf-8'),
            "content_type_id": str(self.content_type_id),
            "horizontal_url": self.horizontal_url,
            "vertical_url": self.vertical_url,
            "function": self.function,
            "organization_id": str(self.organization_id),
            "activated": str(self.activated),
            "label_ids": self.label_ids,
            "play_mode": str(self.play_mode),
            "periods": self.periods,
            "billing_type_play_duration": str(self.billing_type_play_duration),
            "billing_type_play_times": str(self.billing_type_play_times),
            "billing_type_watch_effect": str(self.billing_type_watch_effect),
            "device_id": str(self.device_id),
            "created": str(dt2ts(self.created)),
            "updated": str(dt2ts(self.updated)),
            "start": str(dt2ts(self.start)),
            "finish": str(dt2ts(self.finish)),
            "play_times": str(dt2ts(self.play_times))
        }


class Label(Base):
    __tablename__ = "label"
    id = Column("id", Integer, primary_key=True, doc=u"label_id_seq")
    name = Column('name', String(255), nullable=False, doc=u'标签名')
    industry = Column('industry', String(255), nullable=False, doc=u'')
    category = Column('category', String(255), nullable=False, doc=u'')
    created = Column("created", DateTime, nullable=False, default=datetime.now())
    updated = Column("updated", DateTime, default=datetime.now())

    def to_dict(self):
        return {
            "id": str(self.id),
            "name": self.name,
            "industry": self.industry,
            "category": self.category,
            "created": str(dt2ts(self.created)),
            "updated": str(dt2ts(self.updated))
        }


class FileManager(Base):
    __tablename__ = "file_manager"
    id = Column("id", Integer, primary_key=True, doc=u"file_id")
    url = Column('url', String(255), nullable=False, doc=u'url')
    type = Column('type', String(255), nullable=False, doc=u'')

    def to_dict(self):
        return {
            "id": str(self.id),
            "url": self.url,
            "type": self.type
        }