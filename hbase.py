# coding: utf-8
import happybase
import logging


class Hbase(object):
    """
     :param str name:table name
     :param str row: the row key
     :param list_or_tuple columns: list of columns (optional)
    """

    def __init__(self,host,port=9090):
        self.conn = happybase.Connection(host, port=port, autoconnect=False)
        self.conn.open()

    def list_tables(self):
        tabels = self.conn.tables()
        return tabels

    def table(self, name):
        table = self.conn.table(name)
        return table

    def creat(self, table_name, kw):
        """
        :param name: str
        :param kw: dict
        exp:
            kw = {"":dict()}
        :return: None
        """
        try:
            self.conn.create_table(table_name, kw)
        except Exception as e:
            logging.exception(e)

    def delete(self, name, row):
        table = self.table(name)
        table.delete(row)

    def delete_column(self, name, row, columns):
        self.table(name).delete(row, columns=columns)

    def drop(self, name):
        self.conn.disable_table(name)
        self.conn.delete_table(name)

    def cell(self, name, row, column):
        """
        :return: list
        """
        return self.table(name).cells(row, column)

    def families(self, name):
        """
        :return: dict
        """
        return self.conn.table(name).families()

    def put(self, name, row, kw):
        self.table(name).put(row, kw)

    def get(self, name, row):
        """
        :return: dict
        """
        return self.table(name).row(row)

    def get_column(self, name, row, columns):
        """
        :return: dict
        """
        return self.table(name).row(row, columns)

    def scan(self, name):
        nu = self.conn.table(name).scan()
        datasets = []
        for i in nu:
            datasets.append(i)
        return datasets

    def incr(self, name, row, column):
        self.table(name).counter_inc(row, column=column)

    def dec(self, name, row, column):
        self.table(name).counter_dec(row, column=column)

    def close(self):
        self.conn.close()


if __name__ == "__main__":
    client = Hbase(host="192.168.2.177", port=30036) #Hbase(host="39.108.150.8")
    print(client)
    print(client.list_tables())
    print(client.scan("rec_history"))
    print(client.scan("content"))
    print(client.drop("content"))
    print(client.list_tables())