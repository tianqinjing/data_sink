#!/usr/bin/env python
# encoding: utf-8
# @Time    : 2018/9/29
import hbase
import happybase
from arrange4data import *
# https://192.168.2.177:30090
host = "192.168.2.177" #"127.0.0.1"
port = 30036
client = hbase.Hbase(host, port)

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

db_connection = "postgresql://yinli:yinli@192.168.90.13:5433/yinli"
engine = create_engine(db_connection, echo=False, max_overflow=20)
db_session_maker = sessionmaker(bind=engine, autocommit=False, autoflush=False, expire_on_commit=False)
session = db_session_maker()
print(session)


def init_tables():
    # 新建tables
    create_tables = {
        "content": {     # 广告表
            "info": {},   # content info -> sql(content)
            "content_type": {},
            "device": {},
            "org": {},    # org relative
            "label": {},  # label relative
            "sp": {}      # data snapshot
        },
    }

    for item in create_tables:
        if item not in tables:
            client.creat(item, create_tables[item])


def save_org_data(db_session):
    org_datas = get_org_info(db_session)
    for org in org_datas:
        print(org["info:id"])
        id = org['info:id']
        client.put("organization", str(id), org)


def save_content_to_hbase(db_session):
    contents = get_content_relative(db_session)
    for content in contents:
        print(content)
        row_key = content[0]
        data = content[1]
        print("row_key: {0}. data: {1}".format(row_key, data))
        client.put('content', row_key, data)


if __name__ == "__main__":
    add = False
    delete = False
    client.list_tables()
    print("\n*3")
    if delete:
        client.drop("content")
    else:
        if add:
            init_tables()
            print(client.list_tables())
            save_content_to_hbase(session)
            print("+++++++++++++++++++++++++++++++++")
            print("*********************************")
            print(client.scan("content"))
        else:
            print(client.scan('content'))
