#! /usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/9/12 19:08
from hbase import Hbase
import json
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from backend import Shop, DeviceType, Device, BankType, \
  Bank, Person, ContentType, Content, Label, FileManager

g_catalog_gender = u"性别"
g_catalog_age = u"年龄"
g_catalog_advise = u"广告"
# 性别 0-女 1-男
MALE = "1"
FEMALE = "0"
ALL_GENDER = "2"
gender_map = {u"男": MALE, u"女": FEMALE}
# hbase thrift https://192.168.2.177:30060


def fmt_col_elem(col, data, index=None):
    # 格式化成为hbase的列族
    fmt_data = {}
    for key in data:
        if index is not None:
            fmt_data["%s:%s_%s"%(col, key, str(index))] = data[key]
        else:
            fmt_data["%s:%s" % (col, key)] = data[key]

    return fmt_data


def get_org_info(db_session):
    # 找出所有商店和格式化商店的信息
    org_datas = []
    # 查找所有商店数据
    orgs = db_session.query(Shop).all()
    for org in orgs:
        # 单个商店信息
        org_data = org.to_dict()
        sup_org_data = {}
        org_id = org.id
        # 父机构信息
        sup_organization_id = org.sup_organization_id
        org_data = fmt_col_elem("info", org_data)
        if sup_organization_id:
            sup_org = db_session.query(Shop).filter(Shop.id == sup_organization_id).scalar()
            if sup_org:
                sup_org_data = sup_org.to_dict()
                sup_org = fmt_col_elem("parent_org", sup_org_data)
                org_data.update(sup_org)
        devices = db_session.query(Device).filter(Device.organization_id == org_id).all()

        # 摄像头或者设备ID
        for device in devices:
            index = devices.index(device)
            key = "device:id_%s"%(index+1)
            org_data[key] = device.id
        org_datas.append(org_data)
    return org_datas


def get_device_info(db_session):
    devices = db_session.query(Device).all()
    device_info = []
    for device in devices:
        data = fmt_col_elem("info", device.to_dict())
        device_type_id = device.device_type_id
        device_type = db_session.query(DeviceType.id == device_type_id).scalar()
        if device_type:
            info = fmt_col_elem("device_type", device_type.to_dict())
            data.update(info)
        device_info.append(data)
    return device_info


def get_vip_info(db_session):
    persons = []
    seach_persons = db_session.query(Person).all()
    for search_person in seach_persons:
        person = fmt_col_elem("info", search_person.to_dict())
        bank = db_session.query(Bank).filter(Bank.id == person.bank_id).scalar()
        if bank:
            bank_info = fmt_col_elem("bank", bank.to_dict())
            person.update(bank_info)
        bank_type_id = bank.bank_type_id
        bank_type = db_session.query(BankType).filter(BankType.id == bank_type_id).scalar()
        if bank_type:
            bank_type_info = fmt_col_elem("bank_type", bank_type.to_dict())
            person.update(bank_type_info)
        persons.append(person)
    return persons


# 广告数据sink接口
def get_content_relative(db_session):
    """
    :param db_session: sql db session
    :return:
    """
    import json
    contents = db_session.query(Content).all()
    content_infos = []
    for content in contents:
        # 广告内容
        content_gender = ALL_GENDER
        content_age = "0-100"
        type_id = content.content_type_id
        info = content.to_dict()

        data = fmt_col_elem("info", info)

        # content type
        content_type = db_session.query(ContentType).filter(ContentType.id == type_id).scalar()
        if content_type:
            type_data = content_type.to_dict()
            data.update(fmt_col_elem("content_type", type_data))

        # device info
        device_id = get_device_id(content.device_id)
        device = db_session.query(Device).filter(Device.id == device_id).scalar()
        if device:
            device_data = device.to_dict()
            if device.device_type_id:
                device_type = db_session.query(DeviceType).filter(DeviceType.id == device.device_type_id).scalar()
                if device_type:
                    device_data["device_type_name"] = device_type.name
                    device_data["device_organization_id"] = str(device_type.organization_id)

            data.update(fmt_col_elem("device", device_data))

        # 企业内容
        org = db_session.query(Shop).filter(Shop.id == content.organization_id).scalar()
        org_fmt = fmt_col_elem("org", org.to_dict())
        data.update(org_fmt)

        # label标签
        labels = split_ids(content.label_ids)
        label_data = {}
        has_gender_label = False
        has_age_label = False
        for id in labels:
            index = labels.index(id)
            label = db_session.query(Label).filter(Label.id == id).scalar()
            if label:
                name = label.name
                catalog = label.category
                if catalog == g_catalog_age:
                    age_map = {}
                    # catalog name
                    # 年龄   儿童（0-6）
                    scope_left = u"\uff08"
                    scope_right = u"\uff09"
                    tmp = name.split(scope_left)[-1]
                    tmp = tmp.split(scope_right)[0]
                    value = tmp.split('-')
                    print("=============")
                    print(value)
                    min = 0
                    max = 100
                    if len(value) == 1:
                        min = value[0]
                    elif len(value) == 2:
                        max = value[1]

                    label_data["min_age"] = str(min)
                    label_data["max_age"] = str(max)
                    label_data['age'] = tmp
                    content_age = tmp
                    has_gender_label = True
                elif catalog == g_catalog_gender:
                    # catalog  name
                    # 性别     男/女
                    label_gender = gender_map.get(name, "2")
                    label_data["gender"] = label_gender
                    content_gender = label_gender
                    has_gender_label = True
                else:
                    if catalog not in label_data:
                        label_data[catalog] = []
                    label_data[catalog].append(name)

        if not has_gender_label:
            label_data["gender"] = content_gender

        if not has_age_label:
            label_data["age"] = content_age

        for (key, value) in label_data.items():
            if type(value) is list:
                label_data[key] = json.dumps(value)
        data.update(fmt_col_elem("label", label_data))
        # content_id|年龄|性别
        row_key = "%s|%s|%s"%(info.get("id"), content_age, content_gender)
        snapshot = get_snapshot(data)
        #data["sp:snapshot"] = snapshot
        content_infos.append((row_key,data))
    return content_infos

def get_snapshot(data):
    return [data.keys(), data.values()]

def get_urls_by_ids(db_session, ids):
    if type(ids) is list:
        urls = []
        for id in ids:
            file = db_session.query(FileManager).filter(FileManager.id == int(id)).scalar()
            if file:
                urls.append(file.url)
                print(file.url)
            else:
                urls.append("")
    else:
        urls = ""
        file = db_session.query(FileManager).filter(FileManager.id == int(ids)).scalar()
        if file:
            urls = file.url
    return urls


def split_ids(ids):
    return json.loads(ids)

def get_device_id(str_device_id):
    return json.loads(str_device_id)[0]


if __name__ == "__main__":
    #print(get_org_info(session))
    #print(get_device_info(session))
    db_connection = "postgresql://yinli:yinli@192.168.90.13:5433/yinli"
    engine = create_engine(db_connection, echo=False, max_overflow=20)
    db_session_maker = sessionmaker(bind=engine, autocommit=False, autoflush=False, expire_on_commit=False)
    session = db_session_maker()
    print(session)

    data = get_content_relative(session)

    for dt in data:
        print(dt)
