#! /usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/9/12 13:29
# @File    : base.py

from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()